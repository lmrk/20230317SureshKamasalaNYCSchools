package com.example.a20230317_sureshkamasala_nycschools.ui.base;

public interface BaseView {
    void showLoading();
    void hideLoading();
}

package com.example.a20230317_sureshkamasala_nycschools.ui.schooldetails;

import com.example.a20230317_sureshkamasala_nycschools.data.model.School;
import com.example.a20230317_sureshkamasala_nycschools.data.model.SchoolRating;
import com.example.a20230317_sureshkamasala_nycschools.ui.base.BaseView;

public interface SchoolRatingsDetailsView extends BaseView {

    void showSchoolRating(SchoolRating schoolRating, School school);

    void showErrorMessage();
}

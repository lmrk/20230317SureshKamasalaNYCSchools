package com.example.a20230317_sureshkamasala_nycschools.ui.schoollist;

import com.example.a20230317_sureshkamasala_nycschools.data.model.School;
import com.example.a20230317_sureshkamasala_nycschools.ui.base.BaseView;

import java.util.List;

interface SchoolsView extends BaseView {

    void showSchoolList(List<School> schoolList);

    void showErrorMessage();

}
